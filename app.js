//app.js
var express = require('express')
var app = express();
var server = require('http').Server(app);
// var io = require('socket.io')(server);
var path = require('path');

app.use(express.static(path.join(__dirname, 'public')))
app.get('/', function(req, res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

module.exports = app;
